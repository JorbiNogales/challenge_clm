const mongoose = require('mongoose');
const { Schema } = mongoose;

const RatingSchema = new Schema({
    _movie : { type: Schema.Types.ObjectId, ref: 'movie' },
    source:{
        type: String,
        required: true,
    },
    value: {
        type: String,
        required: true,
    }
})

const model = mongoose.model('rating', RatingSchema);
module.exports = model;