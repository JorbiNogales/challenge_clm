const Router = require("@koa/router");

const movieSchema = require('./../schemas/movie');
const validate = require('./../middlewares/validateData');
const { 
    getAll,
    search,
    replace
} = require('./../controllers/movie');


const router = new Router({
    prefix:'/movies'
});


// SEARCH MOVIE
router.get('/search/:title_param*/:year_param*', search);

// GET CREATE MOVIE
router.get('/:page*',getAll);

// POST REPLACE MOVIE
router.post('/', validate(movieSchema), replace);





module.exports = router;