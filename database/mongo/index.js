const movieModel = require('./models/movie');
const ratingModel = require('./models/rating');

module.exports = { movieModel, ratingModel };