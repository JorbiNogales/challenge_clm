module.exports = {
    MONGO_URI: process.env.MONGO_URI,
    PORT: process.env.PORT,
    API_KEY: process.env.API_KEY,
}