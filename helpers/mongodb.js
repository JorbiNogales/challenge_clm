const mongoose = require('mongoose');
const { MONGO_URI } = require('./../config');


/* CHECK THE CONECTION */
const checkConnection = () => {
    return mongoose.connection.readyState;
}


/* CONNECT */
const connect = async() => {
    try {
        if(!checkConnection()){
            console.log('Connecting...');
            await mongoose.connect(MONGO_URI, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            })
        }
        console.log('CONNECTED SUCCESSFULLY')
    } catch (error) {
        console.log(error);
    }
}

/* DISCONECT */
const disconnect = async () => {
    await mongoose.connection.close();
    return checkConnection();
}

module.exports = { connect, checkConnection, disconnect};