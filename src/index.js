require('dotenv').config();

const { environmentUtils: { validateRequiredEnvs }} = require('./../utils');
const requiredEnvs = ['MONGO_URI', 'PORT'];
validateRequiredEnvs(requiredEnvs);
const { mongoDBHelpers } = require('./../helpers');


(async () => {
    await mongoDBHelpers.connect();
    require('./server');
})();

process.on('SIGINT', () => {
    mongoDBHelpers.disconnect().then((conectionState) => {
        console.log('Database disconect, connection state:', conectionState);
        console.log('Closing process');
        process.exit(0);
    })
})

