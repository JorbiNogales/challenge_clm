const { 
    mongo: { movieModel }, 
} = require('./../../database');
const axios = require('axios').default;
const { API_KEY } = require('./../../config');
const { createRating } = require('./ratings');

// GET ALL MOVIES //
const getAll = async(ctx) =>{
    const { page } = ctx.params
    try {
        movies = await movieModel.paginate({}, { page: page }).then(function(result) { return result });
        ctx.body = movies;
    } catch (error) {
        ctx.body = error;
    }
}

// CREATE MOVIE 
const create = async(data) => {
    const { Title, Year , Genre, Director, Plot, Actors } = data;
    const newMovie = new movieModel({ 
        title: Title,
        year: Year,
        genre: Genre,
        director: Director,
        plot: Plot,
        actors: Actors,
    });
    return await newMovie.save();
}


// SEARCH MOVIE BY API
const search = async(ctx) =>{
    const { title_param, year_param } = ctx.params;
    try {
        let url = `http://www.omdbapi.com/?`;
        if(title_param){
            url = url + 't=' + title_param;
        }
        if(year_param){
            url = url + '&y=' + year_param;
        }
        const response = await axios.get(`${url}&apikey=${API_KEY}`)
        const findMovie = await findMovieByTitle(response.data.Title);
        let resp = findMovie;
        if(findMovie == null){
            if(response.data.Title){
                const movie = await create(response.data)
                await createRating(movie, response.data.Ratings);
                resp = movie;
            } else { 
                resp = "movie not found";
            }
        }
        ctx.body = resp;
    } catch (error) {
        console.error(error);
    }
}


// FIND MOVIE BY TITLE
const findMovieByTitle = async(title) =>{
    return await movieModel.findOne({ title: title});
}

// FIND MOVIE AND REPLACE
const replace = async (ctx) =>{
    try {
        const { title, find, replace} = ctx.request.body;
        const movie = await findMovieByTitle(title);
        if(movie){
            movie.plot = movie.plot.split(find).join(replace);
            movie.save();
        }
        ctx.body = movie;
    } catch (error){
        console.log(error);
    }
}



module.exports = { getAll, create, search, findMovieByTitle, replace};
