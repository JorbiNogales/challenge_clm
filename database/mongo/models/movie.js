const mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
mongoosePaginate.paginate.options = { 
    lean:  true,
    limit: 5,
    populate: 'ratings'
};

const { Schema } = mongoose;


const ActorSchema = new Schema({
        title: {
            type: String,
            required: true,
        },
        year: {
            type: String,
            required: true,
        },
        genre: {
            type: String,
            required: true
        },
        director: {
            type: String,
            required: true
        },
        plot: {
            type: String,
            required: true,
        },
        actors : {
            type: String,
            required: true,
        },
        ratings : [{ type: Schema.Types.ObjectId, ref: 'rating' }]
    }, 
    { timestamps: true}
)

ActorSchema.plugin(require('mongoose-autopopulate'))
ActorSchema.plugin(mongoosePaginate);

const model = mongoose.model('movie', ActorSchema)
module.exports = model;
