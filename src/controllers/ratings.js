const { 
    mongo: { ratingModel }
} = require('./../../database');

const createRating = async  (movie, data) =>{
    const ratings = [];
    data.map(async( rating)=>{
        try {
            ratings.push({
                source: rating.Source,
                value: rating.Value
            });
        } catch (err){
            console.log(err);
        }
    });
    ratingModel.insertMany(ratings).then(async function(rating_list){
        rating_list.map(async (e)=>{
            movie.ratings.push(e);
            movie.save();
        })
    }).catch(function(error){
        console.log(error);
    })
}

module.exports = { createRating }