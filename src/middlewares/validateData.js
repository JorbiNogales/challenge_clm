const Boom = require('@hapi/boom');
module.exports = (schema) => {
  return async (ctx, next) => {
    try {
      await schema.validateAsync(ctx.request.body);
      await next();
    } catch (error) {
      ctx.response.body = Boom.badData(error)
    }
  };
};
