const Koa = require('koa');
const { PORT } = require('./../config');
const body = require('koa-json-body');
const swagger = require("swagger2");
const { ui, validate } = require("swagger2-koa");
const swaggerDocument = swagger.loadDocumentSync("api.yaml");

const app = new Koa();
app.use(body({ limit: '10kb', fallback: true }))


const movieRoutes = require('./routes/movie');
app.use(ui(swaggerDocument, "/swagger"));
app.use(movieRoutes.routes()).use(movieRoutes.allowedMethods());
app.listen(PORT, () =>{
    console.log("SERVER LISTENN ON PORT", PORT)
});

